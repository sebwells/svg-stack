"use strict";

var fs = require('fs');
var path = require('path');

function findSvgsInPath(dir, done) {
    var results = [];

    fs.readdir(dir, function(err, list) {
        if (err)
        {
            return done(err);
        }

        var pending = list.length;
        if (!pending)
        {
            return done(null, results);
        }

        list.forEach(function(file) {

            file = path.resolve(dir, file);

            fs.stat(file, function(err, stat) {
                if (stat && stat.isDirectory()) {
                    findSvgsInPath(file, function(err, res) {
                        results = results.concat(res);

                        if (!--pending)
                        {
                            done(null, results);
                        }
                    });
                } else {
                    if (file.endsWith(".svg"))
                    {
                        results.push(file);
                    }

                    if (!--pending)
                    {
                        done(null, results);
                    }
                }
            });
        });
    });
}

module.exports = findSvgsInPath;