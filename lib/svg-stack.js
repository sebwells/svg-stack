"use strict";

var findSvgsInPath = require('./findSvgsInPath.js');
var readSvg = require('./readSvg.js');
var writeSvg = require('./writeSvg.js');


var containerName = __dirname + "\\stackcontainer.svg";
var outputName = __dirname + "\\..\\test\\stackoutput.svg";

function readContainer(done)
{
    readSvg(containerName, done);
}

function getFilenameWithExtensionFromPath(absoluteFilePath)
{
    var splitName = absoluteFilePath.split("\\");
    return splitName[splitName.length-1];
}

function getFilenameFromPath(absoluteFilePath)
{
    var filename = getFilenameWithExtensionFromPath(absoluteFilePath);
    var splitLastPart = filename.split(".");
    return splitLastPart[0];
}

function addSvgsToContainer(container, done)
{
    findSvgsInPath(process.cwd(), function(err, absoluteFilePathList) {
        if (err)
        {
            console.log(err);
            return;
        }

        absoluteFilePathList = absoluteFilePathList.filter((item) => {
            return (item.endsWith(getFilenameWithExtensionFromPath(containerName)) === false) && (item.endsWith(getFilenameWithExtensionFromPath(outputName)) === false);
        });

        var pending = absoluteFilePathList.length;

        absoluteFilePathList.map((absoluteFilePath) => {
            readSvg(absoluteFilePath,
                (svgObject) => {
                    svgObject.svg.g.map((g) => {
                        g.$ = g.$ === undefined ? {} : g.$;
                        g.$.id = getFilenameFromPath(absoluteFilePath);
                       container.g.push(g);
                    });

                    pending--;

                    if (pending === 0)
                    {
                        done();
                    }
                });
        });
    });
}

function svgStack()
{
    readContainer((container) =>
    {
        container.g = [];
        addSvgsToContainer(container, () =>
        {
            console.dir(container);

            writeSvg(outputName,
                container,
                () => {
                    console.log("done");
                });
        });
    });
}

module.exports = svgStack;

