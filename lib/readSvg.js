"use strict";

var parseString = require('xml2js').parseString;
var fs = require('fs');

function readSvg(absoluteFilePath, done)
{
    fs.readFile(absoluteFilePath, "utf8",
        (err, xml) => {
            parseString(xml, (err, result) => {
                //console.dir(result);
                done(result);
            });
        }
    );
}

module.exports = readSvg;