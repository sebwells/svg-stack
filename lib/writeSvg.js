"use strict";

var fs = require('fs');
var xml2js = require('xml2js');

function writeSvg(absoluteFilePath, object, done)
{
    var builder = new xml2js.Builder();
    var xml = builder.buildObject(object);
    //console.log(xml);

    fs.writeFile(absoluteFilePath, xml, done);
}

module.exports = writeSvg;